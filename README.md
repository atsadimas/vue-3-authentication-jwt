## Notes

#### Install nodejs using nvm
```bash
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash 
nvm install 
```
## Run

```bash
npm run serve -- --port 8081
```

Based on [vue-3-authentication-jwt](https://www.bezkoder.com/vue-3-authentication-jwt/)

# vue-3-authentication-jwt

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
